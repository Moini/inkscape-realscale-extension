# Inkscape Realscale Extension

<img align="left" src="https://cloud.githubusercontent.com/assets/3240233/14927197/c29c2d1e-0e50-11e6-925b-0b8d90ef2a5c.png">

Inkscape extension for Inkscape 1.2 and higher that allows for quick resizing of (architectural) drawings by indicating a line and 
its length in real world units. It can optionally scale the drawing with a specific scale factor and draw a scale rule.

Download most current version from [here](https://gitlab.com/Moini/inkscape-realscale-extension/-/archive/master/inkscape-realscale-extension-master.zip).

If you need an older version, compatible with older Inkscape versions, please visit 
https://gitlab.com/Moini/inkscape-realscale-extension/-/tags and find the version you need.


## Installation: 

Copy the files realscale.py and realscale.inx into the directory indicated in
Edit -> Preferences -> System: User extensions

## Usage:

* Import an architectural drawing / floor plan / map /... into Inkscape or open a file containing one. Make sure it is a single 
  object (group it, if necessary).
* Draw a straight line that connects two points in that drawing of which you know the distance in real life (for example, if you 
  know how long a wall of your house in the drawing is, draw the line from one end of the wall to the other).
* Select the line, then add the drawing to the selection.
* Open the extension dialog: Extensions -> Scaling -> RealScale...
* Enter the length of the line you just drew, as it is in the real world (for example, if your house wall is 10.5 m long, enter 10.50.
* Select the unit you used (for your 10.50 m house, select m; for your 10 cm cardboard box, select cm)
* If you intend to print the drawing, and the original object is bigger than the sheet, consider using a scale factor.
* To do so, first select if you want to use a metric scale factor (based on mulitples of 5) or an imperial one (based on multiples of 2) 
  or if you would like to enter your own scale factor.
* Then, in the corresponding dropdown, or in the number entry field, select or enter the scale you would like to use. 
  The dropdowns only offer scale factors for downscaling. In the 'Custom' field, however, you can also enter values smaller than 1 to 
  upscale a drawing.
* If you would like the scale rule to be drawn on the page, check the option 'Generate Scale Rule'.
* Now choose the number of units the scale rule will comprise. Those will be doubled in the generated scale rule - e.g. it will show 10 cm 
  to the left of the scale rule center (labelled 0) and 10 cm to its right.
* Apply!

## Screenshots

![Extension UI](img/realscale_ui_tab1.png)
![Extension Help Tab](img/realscale_ui_tab2.png)
![Usage example](img/realscale_ui_example_kiel.png)
(Map: [Copyright OpenStreetMap Contributors](http://www.openstreetmap.org/copyright))
